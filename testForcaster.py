import unittest
import numpy as np
import numpy .testing as npt
from datetime import date
from freezegun import freeze_time

import forcaster as F
import dataloader


@freeze_time("2019-07-24 22:01:55")
class testForcasterLate(unittest.TestCase):
    pa=np.asarray([1,2,3,4],dtype=np.float32)
    pa=pa/np.sum(pa)
    pb=np.asarray([4,3,2,1],dtype=np.float32)
    pb=pb/np.sum(pb)
    pc=np.asarray([1,1,1,1],dtype=np.float32)
    pc=pc/np.sum(pc)
    egrid,lgrid=dataloader.loadGrids()
        
    def testTTBrack(self):
        v=F.ttBrack()
        self.assertTrue( v[0]==24)
        self.assertTrue( v[1]==22)

    def testStpd(self):
        self.assertTrue(F.stpd(6)==0)
        self.assertTrue(F.stpd(5)==6)

    def testOptim(self):
        npt.assert_array_almost_equal(
            F.optim([self.pa,self.pb,self.pc]),np.asarray([.4,.3,.3,.4])  
        ) 

    def testPessim(self):
        npt.assert_array_almost_equal(
            F.pessim([self.pa,self.pb,self.pc]),np.asarray([.1,.2,.2,.1])  
        ) 

    def testDayListGenVec(self):
        vals=F.dayListGenVec([2,3,4,5],self.egrid,100,True)
        for v in vals:
            self.assertTrue(v.shape==(10,100))
            self.assertTrue(v[0,0]==1)
        vals=F.dayListGenVec([2,3,4,5],self.lgrid,100,False)
        for v in vals:
            self.assertTrue(v[0,0]==0)

    def testCalcDay(self):
        cfday=F.calcFday()
        self.assertEqual(cfday.day,31)
        self.assertEqual(cfday.month,7)

    def testGenDays(self):
        rvs=F.genDays(self.egrid,self.lgrid,20)
        self.assertEquals(rvs[0][0,0],0)
        self.assertEqual(len(rvs),14)


@freeze_time("2019-07-24 10:01:55")
class testForcasterEarly(unittest.TestCase):
    def testCalcDay(self):
        cfday=F.calcFday()
        self.assertEqual(cfday.day,24)
        self.assertEqual(cfday.month,7)

    def testGenDays(self):
        rvs=F.genDays(self.egrid,self.lgrid,20)
        self.assertEqual(len(rvs),1)
        self.assertEquals(rvs[0][0,0],1)
        



if __name__== '__main__':
    unittest.main()
