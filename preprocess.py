import sys
import csv
from bs4 import BeautifulSoup
from datetime import datetime,date,time,timedelta
import urllib2
import pymc3 as pm
import numpy as np
from pandas import DataFrame
import pickle
import matplotlib.pyplot as plt

off=7

txtI=1
timeI=2
rtwtI=5
def subl(l):
    return [l[txtI],l[timeI],l[rtwtI]]

def remapCsv(fname,newf,mint):
    twts=[]
    with open(fname,'r') as f:
        _=f.next()
        for x in f:
            twts.append(x.split(','))
        
    with open(newf,'w') as f:
        first=True
        for x in twts:
            if not first:
                ttime=datetime.strptime(x[2],'%m-%d-%Y %H:%M:%S')
                if ttime>mint:
                    f.write(','.join(subl(x))+'\n')
                else:
                    break
            else:
                f.write(','.join(subl(x))+'\n')
                first=False

def procTimes(fname,fmt):
    times=[]
    with open(fname) as f:
        reader=csv.reader(f)
        for i,x in enumerate(reader):
            t=datetime.strptime(x[1],fmt)-timedelta(hours=off)
            times.append(t)
    bt=datetime(2016,1,1,0,0)

    return [ [x,x.weekday(),x.hour] for x in times]

def bstv(hour,day):
    hv=hour>12
    return 2*day+hv

def createHists(fname,fmt):
    times=procTimes(fname,fmt)
    eds=[]
    edvs=[]
    lds=[]
    ldvs=[]

    cstate=bstv(times[0][2],times[0][1])
    expnext=(cstate-1)%14
    brackNum=0 
    for t in times:
        if bstv(t[2],t[1])==cstate:
            brackNum+=1
        else:
            hv=(cstate)%2
            if not hv:
                eds.append(brackNum)
                edvs.append(cstate/2)
            else:
                lds.append(brackNum)
                ldvs.append(cstate/2)
            while bstv(t[2],t[1])!=expnext:
                hv=expnext%2
                if not hv:
                    eds.append(0)
                    edvs.append(expnext/2)
                else:
                    lds.append(0)
                    ldvs.append(expnext/2)
                expnext=(expnext-1)%14
            brackNum=1
            cstate=bstv(t[2],t[1])
            expnext=(expnext-1)%14

    evals=[[eds[i] for i in range(len(eds)) if edvs[i]==j ] for j in range(7)]
    lvals=[[lds[i] for i in range(len(lds)) if ldvs[i]==j ] for j in range(7)]

    return evals,lvals
        
        
                
usedSrcs=['Official Schedule','Pool Report']

class tokenManager(object):
    def __init__(self,tstr):
        self.tstr=tstr
        self.state=0

    def match(self,targs):
        if self.tstr in targs:
            self.state=1

class countManager(tokenManager):
    def match(self,targs):
        if self.tstr in targs:
            self.state+=1
inht="In-House"
cpt="Closed Press"
premt="Pre-credentialed Media"
opt="Open Press"
oott="Out-of-Town"
trvlt="Travel"

cm=countManager
tm=tokenManager
            
def analyzeDay(devents):
    fev=devents[0]
    day=int(fev.find('div',{'class':'dayofmonth'}).text)
    myStr=fev.find('div',{'class':'shortdate'}).text.strip().split(u',\xa0')
    month=datetime.strptime(myStr[0],'%B').month
    year=datetime.strptime(myStr[1],'%Y').year
    mngrs1=[cm(inht),cm(cpt),cm(premt),cm(opt),tm(trvlt),tm(oott)]
    mngrs2=[cm(inht),cm(cpt),cm(premt),cm(opt),tm(trvlt),tm(oott)]
    
    for d in devents:
        if not(d.find('td',{'class':'agenda-text'})):
            ptext=d.find('span',{'class':'label'}).text.strip()
            if ptext in usedSrcs:
                rtext=d.find('div',{'class':'pull-right'}).find('small').text
                timestr= d.find('td',{'class':'agenda-time'}).text.strip()
                if 'AM' in timestr:
                    for mngr in mngrs1:
                        mngr.match(rtext)
                else:
                    for mngr in mngrs2:
                        mngr.match(rtext)
                    
    vec1= map(lambda x:x.state,mngrs1)
    vec2= map(lambda x:x.state,mngrs2)
    return date(year,month,day), vec1, vec2

#TODO:handle the daily schedule summary ending june 15 2018 -_-
        
def daySplits(events):
    ci=0
    heads=[]
    eventsByDay=[]
    
    for i,ev in enumerate(events):
        agd= ev.find('td',{'class':'agenda-date'})
        if agd:
            heads.append(i)

    for i in range(len(heads)-1):
        eventsByDay.append(events[heads[i]:heads[i+1]])
    return eventsByDay

def getTags(fname,text=None):
    if not text:
        with open(fname) as f:
            data=f.read()
    else:
        data=text
    pData=BeautifulSoup(data, "lxml")
    evtable= pData.find('td',{'class':'agenda-events'}).parent.parent
    events=evtable.find_all('tr')
    events=filter(lambda x:not (x.find('td',{'class':'agenda-month'})),events)
    devents=daySplits(events)
    tagvs=[analyzeDay(dev) for dev in devents]
    return tagvs

def procTimesSplit(fname,fmt):
    times=[]
    with open(fname) as f:
        reader=csv.reader(f)
        for i,x in enumerate(reader):
            t=datetime.strptime(x[1],fmt)-timedelta(hours=off)
            times.append(t)
    bt=datetime(2016,1,1,0,0)
    ref=time(12,0)
    return [ [x,x.weekday(),x.time()<ref] for x in times]

def prepareData(twn,nn):
    ftime=datetime.combine(date(2017,1,22),time(12,0))
    remapCsv(twn,nn,ftime)

    
def assembleSet(procTwN,calendarName):
    tags=getTags(calendarName)
    nfmt='%m-%d-%Y %H:%M:%S'
    times=procTimesSplit(procTwN,nfmt)

    dayZ=date(2017,1,21)
    dayF=date(2019,6,26)

    earlyDat=[[0,-1] for i in xrange((dayF-dayZ).days+1) ]
    lateDat=[[0,-1] for i in xrange((dayF-dayZ).days+1) ]

    for t in times:
        dd=(t[0].date()-dayZ).days
        if t[2]:
            earlyDat[dd][0]+=1
        else:
            lateDat[dd][0]+=1
        earlyDat[dd][1]=t[1]
        lateDat[dd][1]=t[1]

    for tag in tags:
        dd=(tag[0]-dayZ).days
        earlyDat[dd][1]=tag[0].weekday()
        lateDat[dd][1]=tag[0].weekday()
        earlyDat[dd].append(tag[1])
        lateDat[dd].append(tag[2])
        

    return earlyDat,lateDat

def genvec(fv,dat):
    xs=np.zeros((0,14))
    for d in dat:
        wv=np.zeros(14)
        wv[0]=fv
        wv[d[1]+1]=1
        wv[8:10]=d[2][-2:]
        wv[10:]=d[2][:-2]
        xs=np.append(xs,wv.reshape((1,-1)),axis=0)
    return xs

def createVecs(edat,ldat):

    eys=np.asarray([d[0] for d in edat])
    exs=genvec(1,edat)
    lys=np.asarray([d[0] for d in ldat])
    lxs=genvec(0,ldat)
    ys=np.append(eys,lys)

    xs=np.append(exs,lxs,axis=0)
    return xs,ys

#assumes oldest comes first
def genSdL(edat,bsv,tcd):
    ls=np.asarray(range(len(edat),0,-1),dtype=np.float32)
    sigs=bsv*(ls+tcd)/tcd
    return np.append(sigs,sigs)

def genAvgDay(edat,ldat):
    edGrid=np.zeros((7,6))
    ldGrid=np.zeros((7,6))
    
    cnts=np.zeros((7,1))
    for d in edat:
        cnts[d[1]]+=1
        edGrid[d[1],:2]+=d[2][-2:]
        edGrid[d[1],2:]+=d[2][:-2]
    for d in ldat:
        ldGrid[d[1],:2]+=d[2][-2:]
        ldGrid[d[1],2:]+=d[2][:-2]
    return edGrid/cnts,ldGrid/cnts

def buildAndSample(dv,samps):
    import theano.tensor as tt
    model=None
            
    with pm.Model() as model:
        l=dv[1].shape[0]/2
        stepSize=2000
        trend=pm.AR1('trend',.99,stepSize,shape=l)
        stackTrend=tt.concatenate([trend,trend])

        wtsDay=pm.HalfFlat('wtsDay',shape=7)
        wtsMult=pm.Normal('wtsMult',sd=10.0,mu=0,shape=3)
        
        regvs=tt.dot(dv[0][:,(0,8,9)],wtsMult)+stackTrend
        alpha=pm.HalfFlat('alpha')
        bmu=tt.dot(dv[0][:,1:8],wtsDay)
        mus=bmu*tt.exp(regvs)
        yp=pm.NegativeBinomial('yp',
                               mu=mus,
                               alpha=alpha,
                               shape=dv[1].shape[0],
                               observed=dv[1],
        )

        # pinh=pm.Uniform('pinh',upper=1.0,lower=0.0)
        # ninh=pm.Binomial('ninh',p=pinh,n=dv[0][:,-4],
        #                  shape=dv[1].shape[0]
        # )
        # pcpt=pm.Uniform('pcpt')
        # ppre=pm.Uniform('ppre')
        # popt=pm.Uniform('popt')
        # ncpt=pm.Binomial('ncpt',p=pcpt,n=dv[0][:,-3],
        #                  shape=dv[1].shape[0]
        # )
        # npre=pm.Binomial('npre',p=ppre,n=dv[0][:,-2],
        #                  shape=dv[1].shape[0]
        # )
        # nopt=pm.Binomial('nopt',
        #                  p=popt,n=dv[0][:,-1],
        #                  shape=dv[1].shape[0]
        # )
        
        # y_obs=pm.Normal('y_obs',mu=yp+ninh+ncpt+npre+nopt,
        #                 sd=.5,observed=dv[1]
        # )

    with model:
        trace=pm.sample(samps,cores=1,target_accept=.9,chains=1)
        
    pm.traceplot(trace,varnames=['wtsDay','wtsMult','alpha'])
    plt.show()

    return trace


#goto modelDef

rec=["--tweetclean","--sample","--gengrids","--genHists"]

tfil=None
if __name__=="__main__":
    commands=[]
    for arg in sys.argv[1:]:
        if arg in rec:
            if tfil:
                commands.append(tfil)
            tfil=[arg]
        else:
            try:
                tfil.append(arg)
            except:
                raise Exception('not proper command for preprocess')
    if tfil:
        commands.append(tfil)

    for cmd in commands:
        if cmd[0]=="--tweetclean":
            ftime=datetime.combine(date(2017,1,22),time(12,0))
            remapCsv(cmd[1],cmd[2],ftime)
        if cmd[0]=="--sample":
            print("Reading DataSet \n")
            edat,ldat=assembleSet('trf.csv','calendar_test.html')
            dv=createVecs(edat,ldat)

            print("Generating Model \n")
            trace=buildAndSample(dv,int(cmd[1]))
            trdf=DataFrame(list(trace))
            trace=None
            tDrop=[
                
            ]
            for val in tDrop:
                try:
                    trdf=trdf.drop(columns=[val])
                except:
                    print "{} not found".format(val)
                                
            trdf.to_hdf(cmd[2],'ls')
        if cmd[0]=="--gengrids":
            print("Reading DataSet \n")
            edat,ldat=assembleSet('trf.csv','calendar_test.html')
            dv=createVecs(edat,ldat)
            egrid,lgrid=genAvgDay(edat,ldat)
            import pickle
            with open(cmd[1],'wb') as f:
                pickle.dump([egrid,lgrid],f)
        

        if cmd[0]=="--genHists":
            fmt='%m-%d-%Y %H:%M:%S'
            evals,lvals= createHists(cmd[1],fmt)
            sdict={
                "evals":evals,
                "lvals":lvals,
            }
            with open(cmd[2],'wb') as f:
                pickle.dump(sdict,f)
