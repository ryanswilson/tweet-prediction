all: trf.csv grids.pkl hists.pkl

trf.csv: tr.csv preprocess.py
	python preprocess.py --tweetclean tr.csv trf.csv

grids.pkl: preprocess.py
	python preprocess.py --gengrids grids.pkl

hists.pkl: preprocess.py trf.csv
	python preprocess.py --genHists trf.csv hists.pkl

model: model.h5

model.h5: trf.csv calendar_test.html preprocess.py
	python preprocess.py --sample 1000 model.h5
