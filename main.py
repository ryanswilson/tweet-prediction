import sys
from PySide2 import QtUiTools
from PySide2.QtGui import *
from PySide2.QtWidgets import QApplication  # QLabel
# from PySide2.QtCore import SIGNAL, QObject
# from datetime import date
import dataloader
import forcaster
from os import path

target = '.user/vals.txt'


def numt(widget):
    try:
        return int(float(widget.text()))
    except:
        raise Exception('missing Value')


def wflt(widget):
    try:
        return float(widget.text())
    except:
        raise Exception('missing Value')


class mainWindow(object):
    '''
      '''

    def __init__(self,):
        self.window = QtUiTools.QUiLoader().load("tradingconsole.ui")

        self.generateLists()
        # sets altpl,minshare,maxshares,yesprices,noprices,yescnt,nocnt,psts
        self.modelvs = dataloader.loadModel()
        self.forcman = forcaster.betaForcaster()

        self.curBrack = 0
        self.curNum = 0
        self.setCurNum()
        self.bracks = [None]*7

        self.saveDict = {
            'beta': self.window.betaTxt,
            'cash': self.window.cshEd,
            'marketID': self.window.idEd,
            'startTweets': self.window.stEd,
        }

        self.loadVals('.user/vals.txt')

        self.window.calcBtn.clicked.connect(self.analyze)
        self.window.ldBtn.clicked.connect(self.updatePrice)

        self.window.show()

    def setCurNum(self,):
        '''
        Sets the current number of tweets in the week
        '''
        self.curNum = numt(self.window.twEd)-numt(self.window.stEd)
        if self.curNum < 0:
            raise Exception(
                "Current Number of tweets should be non negative"
            )

    def setCurBrack(self,):
        '''
        set the number of tweets in current half day
        '''
        self.curBrack = numt(self.window.brkEd)

    def genBrackets(self,):
        '''
        sets the brackets based off the text input
        '''
        for i in range(7):
            self.bracks[i] = forcaster.bracket(
                numt(self.minShares[i]), numt(self.maxShares[i]),
                numt(self.yesPrices[i]), numt(self.noPrices[i]),
            )
        return self.bracks

    def analyze(self,):
        '''
        This is the main analysis function,
        cals the forcast portfolio routine
        and updates the relevant displays
        '''
        self.setCurNum()
        self.setCurBrack()
        wbracks = self.genBrackets()
        # TODO: send beta
        yb, nb, lowPs, highPs = self.forcman.returnPort(self.curBrack,
                                        wbracks,
                                        self.curNum,
                                        numt(self.window.pEd)/100.0,
                                        wflt(self.window.betaTxt),
        )
        cash = wflt(self.window.cshEd)
        for i, plbl in enumerate(self.psts):
            pstr = "{:0.3f}".format(lowPs[i])
            apstr = "{:0.3f}".format(highPs[i])
            plbl.setText(pstr)
            self.altPL[i].setText(apstr)
            self.buyNoCnt[i].display(int(cash*nb[i]))
            self.buyYesCnt[i].display(int(cash*yb[i]))

    def updatePrice(self,):
        '''
        This calls the web "api" to update number of tweets and bracket
        prices
        '''
        idn = numt(self.window.idEd)
        predicVals = dataloader.getPrices(idn)
        for cv in predicVals:
            i = cv[4]
            yv = str(int(100*cv[2]))
            nv = str(int(100*cv[3]))
            self.yesPrices[i].setText(yv)
            self.noPrices[i].setText(nv)
            self.minShares[i].setText(str(cv[0]))
            self.maxShares[i].setText(str(cv[1]))

        tweetnum = dataloader.getDnTweetNum()
        self.window.twEd.setText(str(tweetnum))

    def generateLists(self,):
        '''
        creates lists of all the bracket variables
        '''
        self.altPL = [
            self.window.ap1, self.window.ap2, self.window.ap3,
            self.window.ap4, self.window.ap5, self.window.ap6,
            self.window.ap7,
        ]
        self.minShares = [
            self.window.min_1, self.window.min_2, self.window.min_3,
            self.window.min_4, self.window.min_5, self.window.min_6,
            self.window.min_7,
        ]
        self.maxShares = [
            self.window.max_1, self.window.max_2, self.window.max_3,
            self.window.max_4, self.window.max_5, self.window.max_6,
            self.window.max_7,
        ]
        self.yesPrices = [
            self.window.yp_1, self.window.yp_2, self.window.yp_3,
            self.window.yp_4, self.window.yp_5, self.window.yp_6,
            self.window.yp_7,
        ]
        self.noPrices = [
            self.window.np_1, self.window.np_2, self.window.np_3,
            self.window.np_4, self.window.np_5, self.window.np_6,
            self.window.np_7,
        ]
        self.buyNoCnt = [
            self.window.bn_1, self.window.bn_2, self.window.bn_3,
            self.window.bn_4, self.window.bn_5, self.window.bn_6,
            self.window.bn_7,
        ]
        self.buyYesCnt = [
            self.window.by_1, self.window.by_2, self.window.by_3,
            self.window.by_4, self.window.by_5, self.window.by_6,
            self.window.by_7,
        ]
        self.psts = [
            self.window.pd1, self.window.pd2, self.window.pd3,
            self.window.pd4,
            self.window.pd5, self.window.pd6, self.window.pd7,
        ]

    def shutDown(self,):
        '''
        This function is called on window close and handles value saving
        '''
        with open(target, 'w') as f:
            for key in self.saveDict.keys():
                wstr = '{0}:{1}\n'.format(key, self.saveDict[key].text())
                f.write(wstr)

    def loadVals(self, fname):
        '''
        this function is called during the init to set the values to their
        values from the previous session
        '''
        wdict = {}
        if path.exists(fname):
            with open(fname) as f:
                for x in f:
                    vs = x.split(':')
                    if len(vs) == 2:
                        wdict[vs[0]] = vs[1]
            for key in wdict.keys():
                self.saveDict[key].setText(wdict[key])
        else:
            print('settings file does not exist')


def main():
    app = QApplication(sys.argv)
    appWindow = mainWindow()
    app.aboutToQuit.connect(appWindow.shutDown)
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
