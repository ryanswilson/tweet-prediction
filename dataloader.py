import urllib2
import csv
from datetime import datetime,date,time,timedelta
import numpy as np
import pickle
import pandas as pd
from preprocess import getTags
import requests
from bs4 import BeautifulSoup

off=7

def procTimes(fname,fmt):
    times=[]
    with open(fname) as f:
        reader=csv.reader(f)
        for i,x in enumerate(reader):
            t=datetime.strptime(x[1],fmt)-timedelta(hours=off)
            times.append(t)
    bt=datetime(2016,1,1,0,0)

    return [ [x,x.weekday(),x.hour] for x in times]

class TrumpTime():
    def __init__(self,times):
        txs=[x[1] for x in times]
        tys=[x[2] for x in times]
        wg=np.histogram2d(txs,tys,bins=(7,24))
        self.twge=wg[0][:,:12]
        self.twgl=wg[0][:,12:]

    def depth(self):
        ctime=datetime.now()
        wd=ctime.weekday()
        hr=ctime.hour
        fr=ctime.minute/60.0
        if hr>11:
            colv=np.sum(self.twgl[wd,:])
            pv=np.sum(self.twgl[wd,:hr-12])
            if hr==23:
                nv=colv
            else:
                nv=np.sum(self.twgl[wd,:hr-12+1])
        else:
            colv=np.sum(self.twge[wd,:])
            pv=np.sum(self.twge[wd,:hr])
            if hr==11:
                nv=colv
            else:
                nv=np.sum(self.twge[wd,:hr+1])
        return ( (1-fr)*pv+fr*nv)/colv

def tagvToVec(tgv):
    edv=np.zeros(14)
    ldv=np.zeros(14)
    edv[0]=1
    wdv=tgv[0].weekday()
    edv[wdv+1]=1
    ldv[wdv+1]=1
    edv[8:10]=tgv[1][-2:]
    ldv[8:10]=tgv[2][-2:]
    edv[10:]=tgv[1][:-2]
    ldv[10:]=tgv[2][:-2]

    return edv,ldv            

def getCurDayVec():
    contents = urllib2.urlopen("https://factba.se/topic/calendar").read()
    tags=getTags(None,contents)
    ctime=datetime.now().hour

    i=-1
    cv=None
    while cv!=datetime.now().date():
        i+=1
        cv=tags[i][0]

    proc=tagvToVec(tags[i])
    if ctime>=12:
        proc=proc[1:]
    return proc

def loadGrids():
    with open('grids.pkl','rb') as f:
        dat=pickle.load(f)
    egrid=dat[0]
    lgrid=dat[1]
    return egrid,lgrid

def loadHists(fname):
    with open(fname,'rb') as f:
        dat=pickle.load(f)
    return dat['evals'],dat['lvals']

def traceTDict(tr):
    return {
        'alpha':tr['alpha'],
        'wtsMult':tr['wtsMult'],
        'wtsDay':tr['wtsDay'],
        'trend':tr['trend'][-1]
    }

def loadModel():
    df= pd.read_hdf('model.h5')
    tmp=[]
    for ind,tr in df.iloc[:].iterrows():
        tmp.append(traceTDict(tr))
    return tmp
    

class curDayHandle():
    def __init__(self):
        self.last=datetime(2012,1,1,1,1)
        self.pVec=None
        
    def getVec(self):
        td=datetime.now()-self.last
        tdn=td.seconds+100000*td.days
        if tdn>3600:
            self.pVec=getCurDayVec()
            self.last=datetime.now()
        return list( self.pVec)

nfmt='%m-%d-%Y %H:%M:%S'
times=procTimes('trf.csv',nfmt)

tth=TrumpTime(times)
cdh=curDayHandle()

def processName(name):
    if 'or' in name:
        wv=name.split(' or ')
        if 'more' in name:
            return int(wv[0]),10000
        else:
            return 0,int(wv[0])
    else:
        wv=name.split(' - ')
        return int(wv[0]),int(wv[1])

def procPrice(pstr):
    if pstr:
        return float(pstr)
    else:
        return 1.0

def getPrices(idv):
    url="https://www.predictit.org/api/marketdata/markets/{}".format(idv)
    contents=requests.get(url)
    respd=contents.json()['contracts']
    
    vals=[]
    for cntrc in respd:
        yp=procPrice(cntrc['bestBuyYesCost'])
        np=procPrice(cntrc['bestBuyNoCost'])
        dspv=int(cntrc['displayOrder'])
        ll,ul=processName(cntrc['name'])
        vals.append([ll,ul,yp,np,dspv])

    return vals

def getDnTweetNum():
    url="https://twitter.com/realdonaldtrump?lang=en"
    reader=urllib2.urlopen(url)
    contents=reader.read()
    reader.close()
    csoup=BeautifulSoup(contents,'lxml')
    val=csoup.find('li',{'class':'ProfileNav-item--tweets'}).find('a')['title']
    vstr=val.split(' ')[0].replace(',','')
    return int(vstr)
