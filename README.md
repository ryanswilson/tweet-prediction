## Tweet Predictor
This was written to generate a probability distribution for how many tweets Trump would tweet which was then used to place bets on Predictit. This was a complex project that involved web-scraping, Monte Carlo statistics using Hamiltonian Markov chains provided by PyMC3. I additionally wrote a UI that would let me efficiently calculate bet profiles, by trying to combine the Kelly criterion with model uncertainty, to enter into the website as the market evolved.

I am unsure how much of this works I have not used it for a long time.
The actual version control history is in Mercurial as that is what I used at the time.
