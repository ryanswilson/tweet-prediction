import numpy as np
from datetime import datetime,date,time,timedelta
from scipy.optimize import minimize
from scipy.optimize import Bounds
from scipy.special import logsumexp

import dataloader
from dataloader import cdh,tth,loadHists

stride=10

# def forcastDay(dVec,modelV,cnt=1):
#     vals=[]

#     bv=np.dot(modelV['wtsDay'],dVec[1:8])
#     lmv=np.dot(modelV['wtsMult'],dVec[(0,8,9),])
#     mu=bv*np.exp(lmv)

#     n=modelV['alpha']
#     p=mu/(mu+n)

#     tmp=np.random.negative_binomial(n,1-p,size=cnt)
#     for i,key in enumerate(['pinh','pcpt','ppre','popt']):
#         evs=dVec[10+i]
#         if evs<1:
#             tmp+=np.random.binomial(1,evs*modelV[key],size=cnt)
#         else:
#             tmp+=np.random.binomial(evs,modelV[key],size=cnt)
#     return tmp
    

# def forcastWeek(fday,models,egrid,lgrid,cnt=10):
#     preddays=futureDays(egrid,lgrid,fday)
#     cdv=cdh.getVec()
#     if len(cdv)>1:
#         preddays=cdv[1:]+preddays
    
#     cnts=np.zeros(cnt*len(models))
#     for dvec in preddays:
#         tcnt=np.zeros(0)
#         for m in models:
#             tcnt=np.append(tcnt,forcastDay(dvec,m,cnt=cnt))
#         cnts+=tcnt

#     curCnt=np.zeros(0)
#     for m in models:
#         curCnt=np.append(curCnt,forcastDay(cdv[0],m,cnt=cnt))

#     return cnts,curCnt,preddays

# class histCaster():
#     def __init__(self,loadFName,rvn=25):
#         self.evals,self.lvals=loadHists(loadFName)
#         self.rvn=rvn
#     def forcastBracket(self,day,e,cnt):
#         if e:
#             return np.random.choice(self.evals[day][:self.rvn],size=cnt)
#         return np.random.choice(self.lvals[day][:self.rvn],size=cnt)

#     def forcastWeek(self,fday,cnt):
#         tvs=np.zeros(cnt)
#         ctime=datetime.now()
#         cWkd=ctime.weekday()
#         nd=stpd(cWkd)
#         while nd!=2:
#             tvs+=self.forcastBracket(nd,True,cnt)
#             tvs+=self.forcastBracket(nd,False,cnt)
#             print nd
#             nd=stpd(nd)
#         tvs+=self.forcastBracket(2,True,cnt)
#         return tvs


# altForcaster=histCaster('hists.pkl')
# altForcastWeek=altForcaster.forcastWeek

# class forcastHandle():
#     def __init__(self,models,fday,eg,lg,nrun=3):
#         self.tbrack=ttBrack()
#         self.models=models
#         self.fday=fday
#         self.eg=eg
#         self.lg=lg

#         self.aNum=10000
#         self.nrun=3
#         self.cbt=None
        
#         self.changed=True
#         self.achanged=True
#         self.forcast=None
#         self.cbForc=None
#         self.aForcast=None

#     def getAltForcast(self,cbt):
#         if self.cbt!=cbt:
#             self.achanged=True
#             self.cbt=cbt
#         if self.tbrack!=ttBrack():
#             self.achanged=True
#             self.tbrack=ttBrack()

#         if self.achanged:
#             self.aForcast=altForcastWeek(self.fday,len(self.forcast[0]))
#             self.achanged=False
#             # self.cbForc=ttForcast(self.cbt,len(self.forcast[0]))

#         return self.aForcast

#     def getForcast(self,cbt):
#         if self.cbt!=cbt:
#             self.changed=True
#             self.cbt=cbt
#         if self.tbrack!=ttBrack():
#             self.changed=True
#             self.tbrack=ttBrack()
        
#         if self.changed:
#             self.forcast=forcastWeek(self.fday,self.models,
#                                      self.eg,self.lg,self.nrun
#             )
#             self.changed=False
#             self.cbForc=ttForcast(self.cbt,len(self.forcast[0]))
            
#         return self.forcast[0],self.cbForc

#     def returnPort(self,cbt,brackets,curNum,ppv):
#         fpred,cpred=self.getForcast(cbt)
#         apred=self.getAltForcast(cbt)
#         estPs=estBrackPs(fpred,cpred,brackets,curNum)
#         altPs=estBrackPs(apred,cpred,brackets,curNum)

#         res=optsplit(estPs,altPs,brackets,ppv)
#         return res.x[:7],res.x[7:],estPs,altPs

class bracket():
    def __init__(self,minv,maxv,yp,np):
        self.minv=minv
        self.maxv=maxv
        self.yp=yp/100.0
        self.np=np/100.0

    def up(self,yp,np):
        self.yp=yp/100.0
        self.np=np/100.0

    def has(self,val):
        return (val>=self.minv) and (val<=self.maxv)

    def __repr__(self):
        return '{0} {1} {2} {3}'.format(self.minv,self.maxv,self.yp,self.np)


def ttBrack():
    ctime=datetime.now()
    v1=ctime.day
    v2=ctime.hour
    return (v1,v2)

def stpd(wkdv):
    return (wkdv+1)%7

#TODO: fix optim pessim
def optim(modelPs):
    return np.max(np.asarray(modelPs),axis=0)

def pessim(modelPs):
    return np.min(np.asarray(modelPs),axis=0)


def dayListGenVec(dayList,grid,count,early):
    pdays=[]
    for d in dayList:
        wv=np.zeros(10).reshape((10,1))
        if early:
            wv[0]=1
        wv[d+1]=1
        war=np.tile(wv,(1,count))
        pTrav=grid[d,0]
        pOOT=grid[d,1]/pTrav

        tempTrav=np.zeros(count)
        tempTrav[np.random.uniform(tempTrav)<pTrav]=1
        tempOOT=np.zeros(count)
        tempOOT[np.random.uniform(tempOOT)<pOOT]=1
        tempOOT=tempTrav*tempOOT

        war[8,:]=tempTrav
        war[9,:]=tempOOT
        pdays.append(war)
    return pdays

def calcFday():
    ctime=datetime.now()
    wdv=datetime.now().weekday()
    if wdv!=2:
        delt=(2-wdv)%7
    elif ctime.hour<12:
        delt=0
    else:
        delt=7
    fday=ctime+timedelta(days=delt)
    return fday.date()
        
def genDays(egrid,lgrid,Nc=1000):
    fday=calcFday()
    ctime=datetime.now()
    delt=(fday-ctime.date()).days
    cwv=ctime.weekday()

    cHDE=ctime.hour<12
    if delt==0:
        eDays=[cwv]
        lDays=[]
    else:
        eDays=[]
        lDays=[]
        if cHDE:
            eDays.append(cwv)
        lDays.append(cwv)
        cwv=(cwv+1)%7
        for i in xrange(delt-1):
            eDays.append(cwv)
            lDays.append(cwv)
            cwv=(cwv+1)%7
        eDays.append(cwv)
    #the only ordering that matters is that the current partial day is first
    pdays=[]
    if cHDE:
        pdays+=dayListGenVec(eDays,egrid,Nc,True)
        pdays+=dayListGenVec(lDays,lgrid,Nc,False)
    else:
        pdays+=dayListGenVec(lDays,lgrid,Nc,False)
        pdays+=dayListGenVec(eDays,egrid,Nc,True)
    return pdays

def estBrackPs(num,brackets):
    vals=num
    cnts=np.zeros(len(brackets))
    for v in vals:
        for i,b in enumerate( brackets):
            if b.has(v):
                cnts[i]+=1
    return cnts/np.sum(cnts)


class oneModelForcast():
    def __init__(self,mdlTrace):
        self.wtd=mdlTrace['wtsDay']
        self.alpha=mdlTrace['alpha']
        self.trend=mdlTrace['trend']
        self.wtsMult=mdlTrace['wtsMult']

        self.dLen=-1
        self.ftrCnts=None
        self.curHDTweet=None
        self.ldepth=-1
        self.lastCnt=None
        self.lastCurHDPred=None
        self.lastps=None

    def getCntsDay(self,dVec,nobs=0,f=0.0):
        bvs=np.dot(self.wtd,dVec[1:8,:])
        lmvs=np.dot(self.wtsMult,dVec[(0,8,9),:])
        #r_new=r_old+N_obs
        r=self.alpha+nobs
        mus=bvs*np.exp(lmvs+self.trend)
        #p_new=(1-f)*p_old
        ps=mus/(mus+self.alpha)
        ps*=(1-f)
        tmp=np.random.negative_binomial(r,1-ps,)
        return tmp

    #first Dvec is current time brack
    def getWeekPs(self,brackets,dVecs,nobs,curCnt):

        cond1= (self.curHDTweet==nobs) and (abs(tth.depth()-self.ldepth)<.05)
        cond2= (len(dVecs)==self.dLen)
        cond3= (self.lastCnt==curCnt)

        self.lastCnt=curCnt
        #three parts, currrent count, current hd, future hds
        if cond1 and cond2 and cond3:
            return self.lastps
        else:
        
            if cond1:
                cnt=np.copy(self.lastCurHDPred)
            else:
                cnt=self.getCntsDay(dVecs[0],nobs,tth.depth())
                self.lastCurHDPred=np.copy(cnt)
                self.curHDTweet=nobs
                self.ldepth=tth.depth()

            if cond2:
                cnt+=self.ftrCnts
            else:
                self.ftrCnts=np.zeros(dVecs[0].shape[1],dtype=np.int64)
                for dVec in dVecs[1:]:
                    self.ftrCnts+=self.getCntsDay(dVec)
                cnt+=self.ftrCnts
                self.dLen=len(dVecs)
            self.lastps=estBrackPs(cnt+curCnt,brackets)
        return self.lastps


def portfolioValue(ps,brackets,sShares,fShares,pdim):
    l=len(ps)
    acc=0
    sps=np.asarray([b.yp for b in brackets])
    spsv=sps*sShares
    fps=np.asarray([b.np for b in brackets])
    subvals=[]
    for i,pi in enumerate(ps):
        mask=np.ones(l,dtype=bool)
        mask[i]=0
        costs=np.sum(spsv[mask])+fShares[i]*fps[i]
        sben=.9*(1-sps[i])*sShares[i]
        fben=.9*np.sum( (fShares*(1-fps))[mask] )
        bval=np.log(1-costs+sben+fben) 
        acc+=pi*bval
        subvals.append(bval)
    paraV=(1-pdim)*acc+pdim*np.min(subvals)
    return paraV
    
def optsplit(modelps,brackets,beta,para=0.0,tvaltest=False):
    num=len(modelps)
    l=7
    x0=np.zeros(2*l)
    def tval(x):
        vals=[-beta*portfolioValue(ps,brackets,x[:l],x[l:],para) for ps in modelps ]
    
        return logsumexp(np.asarray(vals))
    if tvaltest:
        return tval
    
    ubv=np.asarray([np.inf]*2*l)
    bnd=Bounds(np.zeros(2*l),ubv)
    res=minimize(tval,x0,method='SLSQP',bounds=bnd)
    return res

class betaForcaster():
    def __init__(self):
        self.egrid,self.lgrid=dataloader.loadGrids()
        modelTraces=dataloader.loadModel()[::stride]
        self.models=[]
        for trace in modelTraces:
            self.models.append(oneModelForcast(trace))
        
    def returnPort(self,curHDTweet,brackets,curNum,ppv,beta):
        days=genDays(self.egrid,self.lgrid)
        modelps=[model.getWeekPs(brackets,days,curHDTweet,curNum) for model in self.models]
        res=optsplit(modelps,brackets,beta)
        
        return res.x[:7],res.x[7:], pessim(modelps),optim(modelps)
